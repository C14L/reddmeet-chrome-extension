(function() {

  var baseUserURL = 'https://reddmeet.com/u/';
  //var baseUserURL = 'http://redddate.com/u/';

  var elHtml = document.getElementsByTagName('html')[0];
  var elHead = document.getElementsByTagName('head')[0];
  var elBody = document.getElementsByTagName('body')[0];
  var elContent = elBody.querySelector('div.content[role="main"]');

  var iconPng = chrome.extension.getURL('/icon.png');
  var loadingGif = chrome.extension.getURL('/loading.gif');

  chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    if (msg.text === 'page_fadeout') {
      // This needs to be called as early as possible to hide the page, because
      // for some reason, it starts to lose style even before we do anything with
      // the page's stylesheets.
      //
      // This fades only a virgin page, so its possible to open and close the
      // popup repeatedly, without re-rendering the page every time.
      if ( ! isPageDone()) {
        bodyFadeOut();
      }
      sendResponse(true);
    }

    if (msg.text === 'page_fadein') {
      // Simply resets the opacity of the <body/> back to "1" and the background
      // color of the <html/> element back to white.
      bodyFadeIn();
      sendResponse(true);
    }

    if (msg.text === 'get_usernames') {
      var usernames = getUsernames();
      sendResponse(usernames);
    }

    if (msg.text === 'mark_members') {
      markMemberUsers(msg.userlist);
      addStyle();
      sendResponse(true);
    }
  });

  function bodyFadeOut() {
    // Fade out entire <body/> and display a "loading spinner" on <html/> background.
    //elBody.style.opacity = '0';
    //elBody.style.transition = '0.3s';
    elBody.style.visibility = 'hidden';
    elHtml.style.background = '#FFF url(' + loadingGif + ') center center no-repeat';
  }

  function bodyFadeIn() {
    //elBody.style.opacity = "1";
    //elBody.style.transition = '0.3s';
    elBody.style.visibility = 'visible';
    elHtml.style.backgroundColor = "#FFF";
  }

  function getUsernames() {
    // Return usernames from cache or find them in the markup.
    if (isPageDone()) {
      return getCachedUsernames();
    } else {
      var usernames = findRedditUsernames();
      cacheUsernames(usernames);
      return usernames;
    }
  }

  function cacheUsernames(usernames) {
    // Cache usernames in a meta tag.
    elHead.innerHTML += '<meta name="redddate-page-done" content="' + usernames.join(' ') + '">';
  }

  function getCachedUsernames() {
    // Return usernames from cache or null.
    var s = elHead.querySelector('meta[name="redddate-page-done"]').content;
    //console.log('content.js: getCachedUsernames(): s == ' + s);
    return (s) ? s.split(' ') : null;
  }

  function isPageDone() {
    // Checks if the page is done already.
    return !!elHead.querySelector('meta[name="redddate-page-done"]');
  }

  function findRedditUsernames() {
    var all = document.all[0].querySelectorAll('div.content[role="main"] p.tagline a.author');
    var li = [];
    for (var i=0; i<all.length; i++) li.push(all[i].innerHTML);
    return li;
  }

  function addStyle() {
    var s = '' +
      '.reddmeet-link { ' +
      '   background-color: #005280; '+
      '   border: 0; outline: 0; ' +
      '   border-radius: 2px; ' +
      '   box-sizing: padding-box; ' +
      '   color: #FFFFFF; ' +
      '   display: inline-block; height: 24px; ' +
      '   line-height: 23px; ' +
      '   margin: 0; padding: 0; ' +
      '   overflow: hidden; ' +
      '   vertical-align: middle; ' +
      '}  ' +
      '   ' +
      '.reddmeet-link a.author { ' +
      '   color: inherit; border: 0; '+
      '}  ' +
      '   ' +
      '.reddmeet-icon { ' +
      '   display: inline-block; width: 32px; height: 24px; ' +
      '   background: #005280 url(http://redddate.com/static/icon32.png) center center no-repeat; ' +
      '   background-size: cover; ' +
      '   border: 0; border-right: 8px solid transparent; ' +
      '   margin: 0; padding: 0; ' +
      '   outline: 0; ' +
      '   vertical-align: middle; ' +
      '}  ' +
      '   ' +
      '.reddmeet-reddit-icon { ' +
      '   background: #E0E0E0 url(https://www.redditstatic.com/icon.png) center center no-repeat; ' +
      '   background-size: cover; ' +
      '   border: 2px solid transparent; ' +
      '   display: inline-block; width: 20px; height: 20px; ' +
      '   line-height: inherit; ' +
      '   margin: 0 0 0 8px; padding: 0; ' +
      '   outline: 0; ' +
      '   overflow: hidden; ' +
      '   vertical-align: middle; ' +
      '}  ' +
      '   ';

    //document.getElementsByTagName('style')[0].innerText += s;
    if ( ! document.getElementById('reddmeet-style')) {
      elHead.innerHTML += '<style id="reddmeet-style">' + s + '</style>';
    }
  }

  function markMemberUsers(userlist) {
    for (var i=0; i<userlist.length; i++) { // loop through all the userlist objects.
      var qs = 'a.author[href="https://www.reddit.com/user/' + userlist[i]['name'] + '"]';
      var qr = elContent.querySelectorAll(qs);

      if (qr && qr.length > 0) { // was there at least one element for this username?
        for (var j=0; j<qr.length; j++) { // loop all the occurences of this username on the page.
          if ( ! qr[j].parentNode.classList.contains('reddmeet-link')) { // only if its not already wrapped.
            var url = baseUserURL + userlist[i]['name'];
            var redditLink = '<a class="reddmeet-reddit-icon" href="' + qr[j].href + '"></a>'
            qr[j].href = url;
            qr[j].outerHTML = '<span class="reddmeet-link"><a class="reddmeet-icon" href="' + url + '" style="background-image:url(' + (userlist[i]['pic']||iconPng) + ')"></a>' + qr[j].outerHTML + redditLink + '</span>';
          }
        }
      }
    }
  }

})();