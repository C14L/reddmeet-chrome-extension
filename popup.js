(function() {

  Object.prototype.getName = function() {
     var funcNameRegex = /function (.{1,})\(/;
     var results = (funcNameRegex).exec((this).constructor.toString());
     return (results && results.length > 1) ? results[1] : "";
  };

  // http://stackoverflow.com/a/13486540/5520354
  function unique_set(arr) {
    return arr.filter(function (e, i, arr) {
      return arr.lastIndexOf(e) === i;
    });
  }

  Array.prototype.contains = function(obj) {
    return !(this.indexOf(obj) < 0);
  }

  ///////////////////////////////////////////////////////////////////////

  var DEBUG = true;

  //var baseUrl = 'http://localhost:8000';
  //var baseUrl = 'http://redddate.com';
  var baseUrl = 'https://reddmeet.com';

  var baseUserURL = baseUrl + '/u/';
  var baseApiUrl = baseUrl + '/api/v1/';

  var userCacheHandle = 'reddmeetUsers';

  var iconPng = 'icon.png';
  var loadingGif = 'loading.gif';

  var isBusy = false;  // only fetch one tab at a time.
  var currTab = null;  // the tab we are currently working on.

  document.addEventListener('DOMContentLoaded', function() {
    getCurrentTabUrl(function(tab) {
      if (!isBusy) {
        if (isRedditUrl(tab.url)) {
          // Only do anything if we are not already
          // busy and the tab contains a Reddit URL.
          isBusy = true;
          currTab = tab;

          // Hide the <body/> to avoid re-rendering that seems to
          // happen for no reason.
          chrome.tabs.sendMessage(currTab.id, {text: 'page_fadeout'}, function(response){ });
          setTimeout(function() {
            chrome.tabs.sendMessage(currTab.id, {text: 'get_usernames'}, receiveUserlist);
          }, 300);
          setTimeout(function() {
            chrome.tabs.sendMessage(currTab.id, {text: 'page_fadein'}, function(response){ });
          }, 900);

        } else {
          // not reddit
          replaceStatus('Visit a reddit comment thread.')
        }
      } else {
        //replaceStatus();
      }
    });
  });

  ///////////////////////////////////////////////////////////////////////

  function isRedditUrl(url) {
    var re = new RegExp("^https?://(\w+\.)+reddit.com(:\d+)?[$/]");
    return url.match(re);
  }

  function getCurrentTabUrl(callback) {
    var queryInfo = { active: true, currentWindow: true, };

    chrome.tabs.query(queryInfo, function(tabs) {
      var tab = tabs[0];
      callback(tab);
    });
  }

  function receiveUserlist(userlist) {
    if (typeof userlist == 'undefined' || userlist.getName() != 'Array') { userlist = []; }

    if (userlist.length === 0) {
      //replaceStatus('popup.js: Got empty userlist, do nothing.');
      //chrome.tabs.sendMessage(currTab.id, {text: 'page_fadein'}, function(response){ });
    }
    else {
      userlist = unique_set(userlist); // remove duplicates.

      if (userlist.length == 0) {
        //replaceStatus('popup.js: Got empty userlist, do nothing.');
        //chrome.tabs.sendMessage(currTab.id, {text: 'page_fadein'}, function(response){ });
      } else {
        // with the list of usernames, check which have a reddmeet account
        // and pass the resulting list on to the markMembers function.
        queryReddmeetUsernames(userlist).then(markMembers);
      }
    }
  }

  function markMembers(userlist) {
    // receives a list of usernames that are members of reddmeet.
    // mark them in the page's HTML and render them as a list in
    // the popup bubble.
    //
    // userlist = [{ 'name': 'username', 'pic': 'http://...' }, { ... }, ]

    var html = '';

    if (userlist.length > 0) {
      chrome.tabs.sendMessage(currTab.id, {text: 'mark_members', 'userlist': userlist}, function(response){
        //chrome.tabs.sendMessage(currTab.id, {text: 'page_fadein'}, function(response){ });
      });

      for (var i=0; i<userlist.length; i++) {
        var ahref = '<a target="_blank" href="' + baseUserURL + userlist[i]['name'] + '">';
        var pic = '<span class="pic" style="background-image: url(' + (userlist[i]['pic']||iconPng) + ')"></span>';
        var infoLine1 = '<span class="infoline1">' + userlist[i]['name'] + '</span>';
        var infoLine2 = '<span class="infoline2">' + userlist[i]['age'] + ', ' + userlist[i]['sex'] + '</span>';
        var infoLine3 = '<span class="infoline3">' + ( userlist[i]['dist'] ? userlist[i]['dist'] + ' away' : 'Login to see distance...' ) + '</span>';

        html += '<li>' + ahref + pic + infoLine1 + infoLine2 + infoLine3 + '</a></li>';
      }
      replaceStatus('<ul id="userlist">' + html + '</ul>');
      isBusy = false;
    }
    else {
      replaceStatus('<p id="userlist-empty">there is nobody</p>');
      isBusy = false;
    }
  }

  function clearStatus() {
    document.getElementById('status').innerHTML = '';
  }

  function addStatus(statusText) {
    document.getElementById('status').innerHTML += '<p>' + statusText + '</p>';
  }

  function replaceStatus(statusText) {
    document.getElementById('status').innerHTML = statusText;
  }

  function queryReddmeetUsernames(userlist) {
    // For a list of usernames, query the reddmeet API if they have an account.
    // The API returns a list of usernames that do have a reddmeet profile.
    //
    // Uses sessionCache to avoid fetching the same usernames twice.
    //
    return new Promise(function(resolve, reject) {
      // Try to fetch users from localStorage.

      var cache = JSON.parse(localStorage.getItem(userCacheHandle)) || [];
      var userlistComplete = []; // collect here all items wuth full data from cache.
      var usernamesTodo = []; // collect here all items that are sent to reddmeet server.

      for (var j=0; j<userlist.length; j++) {
        var isCached = false;

        if (cache.length > 0){
          for (var i=0; i<cache.length; i++) {
            if (cache[i]['name'] == userlist[j]) {
              userlistComplete.push(cache[i]); // Found one!
              isCached = true;
              break;
            }
          }
        }

        if (!isCached) {
          usernamesTodo.push(userlist[j]); // add to "sent to server" list
        }
      }

      if (usernamesTodo.length == 0) {
        resolve(userlistComplete); // nothing to send to server?
      }

      // Only get fresh data for those users not found in localStorage.
      var apiUrl = baseApiUrl + 'filter-members.json?userlist=' + encodeURIComponent(usernamesTodo.join(' '));
      var xhr = new XMLHttpRequest();
      xhr.open("GET", apiUrl, true);
      xhr.onreadystatechange = function() {
        if (xhr.readyState == xhr.DONE) {
          if (xhr.status == 200) {
            var data = JSON.parse(xhr.responseText);
            // These users are not yet in the cache, add them. Do that inline and
            // re-use the "cache" variable, so we don't need to fetch the localStorage
            // again, just to append these values.
            localStorage.setItem(userCacheHandle, JSON.stringify(cache.concat(data)));
            // Now merge them with the users we previously found in the cache.
            var userlist = data['userlist'].concat(userlistComplete);
            resolve(userlist);
          } else
          if (xhr.status == 404) {
            // In DEBUG mode, marks all users as members, otherwise no user.
            resolve(mockUserlist(usernamesTodo));
          }
        }
      }
      xhr.send();
    });
  }

  function mockUserlist(usernames) {
    // Return a fake list of user objects with pics if DEBUG, else an empty list.
    var response = [];
    var fakepics = ['https://i.imgur.com/JuJwWLCs.jpg', 'https://i.imgur.com/WgfwLfIs.jpg',
                    'https://i.imgur.com/P02Ejbfs.jpg', 'https://i.imgur.com/50z3fWPs.jpg',
                    'https://i.imgur.com/TSmS03Ds.jpg', 'https://i.imgur.com/CJQmX50s.jpg'];

    if (DEBUG) {
      for (var i=0; i<usernames.length; i++) {
        var p = Math.floor(i % fakepics.length);
        response.push({ 'name': usernames[i], 'pic': fakepics[p] });
      }
    }

    return response;
  }

})();